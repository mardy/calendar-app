# Uyghur translation for ubuntu-calendar-app
# Copyright (c) 2013 Rosetta Contributors and Canonical Ltd 2013
# This file is distributed under the same license as the ubuntu-calendar-app package.
# Gheyret Kenji <gheyret@gmail.com>, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: ubuntu-calendar-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-04-20 04:27+0000\n"
"PO-Revision-Date: 2017-08-19 08:34+0000\n"
"Last-Translator: qutghan <qutghan@outlook.com>\n"
"Language-Team: Uighur <https://ubpweblate.tnvcomp.com/projects/ubports/"
"calendar-app/ug/>\n"
"Language: ug\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 2.15\n"
"X-Launchpad-Export-Date: 2017-04-05 07:14+0000\n"

#: ../qml/AllDayEventComponent.qml:89 ../qml/TimeLineBase.qml:50
msgid "New event"
msgstr ""

#. TRANSLATORS: Please keep the translation of this string to a max of
#. 5 characters as the week view where it is shown has limited space.
#: ../qml/AllDayEventComponent.qml:148
msgid "%1 event"
msgid_plural "%1 events"
msgstr[0] ""
msgstr[1] ""

#. TRANSLATORS: the argument refers to the number of all day events
#: ../qml/AllDayEventComponent.qml:152
msgid "%1 all day event"
msgid_plural "%1 all day events"
msgstr[0] "%1 دانە پۈتۈن كۈنلۈك ھادىسە"

#: ../qml/RemindersModel.qml:31 ../qml/RemindersModel.qml:99
msgid "No Reminder"
msgstr "ئەسكەرتكۈچ يوق"

#. TRANSLATORS: this refers to when a reminder should be shown as a notification
#. in the indicators. "On Event" means that it will be shown right at the time
#. the event starts, not any time before
#: ../qml/RemindersModel.qml:34 ../qml/RemindersModel.qml:103
msgid "On Event"
msgstr "ئىجىرا بۇلىۋاتىدۇ"

#: ../qml/RemindersModel.qml:43 ../qml/RemindersModel.qml:112
msgid "%1 week"
msgid_plural "%1 weeks"
msgstr[0] ""
msgstr[1] ""

#: ../qml/RemindersModel.qml:54 ../qml/RemindersModel.qml:110
msgid "%1 day"
msgid_plural "%1 days"
msgstr[0] ""
msgstr[1] ""

#: ../qml/RemindersModel.qml:65 ../qml/RemindersModel.qml:108
msgid "%1 hour"
msgid_plural "%1 hours"
msgstr[0] ""
msgstr[1] ""

#: ../qml/RemindersModel.qml:74
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] ""
msgstr[1] ""

#: ../qml/RemindersModel.qml:104 ../qml/RemindersModel.qml:105
#: ../qml/RemindersModel.qml:106 ../qml/RemindersModel.qml:107
#: ../qml/SettingsPage.qml:304 ../qml/SettingsPage.qml:305
#: ../qml/SettingsPage.qml:306 ../qml/SettingsPage.qml:307
#: ../qml/SettingsPage.qml:308 ../qml/SettingsPage.qml:309
#: ../qml/SettingsPage.qml:310 ../qml/SettingsPage.qml:311
#, fuzzy
msgid "%1 minutes"
msgstr "15 مىنۇت"

#: ../qml/RemindersModel.qml:109
#, fuzzy
msgid "%1 hours"
msgstr "1 سائەت"

#: ../qml/RemindersModel.qml:111
#, fuzzy
msgid "%1 days"
msgstr "1 كۈن"

#: ../qml/RemindersModel.qml:113
#, fuzzy
msgid "%1 weeks"
msgstr "1 ھەپتە"

#: ../qml/RemindersModel.qml:114
msgid "Custom"
msgstr ""

#: ../qml/calendar.qml:80
msgid ""
"Calendar app accept four arguments: --starttime, --endtime, --newevent and --"
"eventid. They will be managed by system. See the source for a full comment "
"about them"
msgstr ""
"يىلنامە ئېپى 4 پارامېتىر قوبۇل قىلىدۇ: --starttime, --endtime, --newevent "
"and --eventid. بۇلار سىستېما تەرىپىدىن باشقۇرۇلۇشى مۇمكىن. بۇنىڭ تەپسىلىي "
"چۈشەندۈرۈلۈشى ئۈچۈن ئەسلى كودى قاراڭ."

#: ../qml/calendar.qml:341 ../qml/calendar.qml:522 ../qml/AgendaView.qml:51
msgid "Agenda"
msgstr "كۈنتەرتىپ"

#: ../qml/calendar.qml:349 ../qml/calendar.qml:543
msgid "Day"
msgstr "كۈن"

#: ../qml/calendar.qml:357 ../qml/calendar.qml:564
msgid "Week"
msgstr "ھەپتە"

#: ../qml/calendar.qml:365 ../qml/calendar.qml:585
msgid "Month"
msgstr "ئاي"

#: ../qml/calendar.qml:373 ../qml/calendar.qml:606
msgid "Year"
msgstr "يىل"

#: ../qml/calendar.qml:718 ../qml/EventDetails.qml:173
#: ../qml/TimeLineHeader.qml:66
msgid "All Day"
msgstr "كۈن بويى"

#: ../qml/ColorPickerDialog.qml:25
msgid "Select Color"
msgstr "رەڭ تاللا"

#: ../qml/ColorPickerDialog.qml:55 ../qml/OnlineAccountsHelper.qml:73
#: ../qml/RemindersPage.qml:88 ../qml/NewEvent.qml:394
#: ../qml/DeleteConfirmationDialog.qml:63
#: ../qml/EditEventConfirmationDialog.qml:52
msgid "Cancel"
msgstr "ئەمەلدىن قالدۇر"

#: ../qml/AgendaView.qml:101
msgid "You have no calendars enabled"
msgstr "ھېچقانداق يىلنامە ئىناۋەتلىك ئەمەس"

#: ../qml/AgendaView.qml:101
msgid "No upcoming events"
msgstr "پىلاندا ھادىسىلەر يوق"

#: ../qml/AgendaView.qml:113
msgid "Enable calendars"
msgstr "يىلنامىلەرنى ئىناۋەتلىك قىل"

#: ../qml/AgendaView.qml:206
msgid "no event name set"
msgstr ""

#: ../qml/AgendaView.qml:208
msgid "no location"
msgstr ""

#. TRANSLATORS: this refers to how often a recurrent event repeats
#. and it is shown as the header of the page to choose repetition
#. and as the header of the list item that shows the repetition
#. summary in the page that displays the event details
#: ../qml/EventRepetition.qml:40 ../qml/EventRepetition.qml:179
msgid "Repeat"
msgstr "قايتىلا"

#: ../qml/EventRepetition.qml:199
msgid "Repeats On:"
msgstr "تەكرارلىنىش:"

#: ../qml/EventRepetition.qml:245
#, fuzzy
msgid "Interval of recurrence"
msgstr "X قېتىم تەكرارلانغاندىن كېيىن"

#: ../qml/EventRepetition.qml:270
msgid "Recurring event ends"
msgstr "تەكرارلىنىشنىڭ ئاخىرلىشىشى"

#. TRANSLATORS: this refers to how often a recurrent event repeats
#. and it is shown as the header of the option selector to choose
#. its repetition
#: ../qml/EventRepetition.qml:294 ../qml/NewEvent.qml:840
msgid "Repeats"
msgstr "قايتىلىنىش"

#: ../qml/EventRepetition.qml:320 ../qml/NewEvent.qml:494
msgid "Date"
msgstr "چېسلا"

#: ../qml/YearView.qml:57 ../qml/WeekView.qml:60 ../qml/DayView.qml:76
#: ../qml/MonthView.qml:51
msgid "Today"
msgstr "بۈگۈن"

#: ../qml/YearView.qml:83
msgid "Year %1"
msgstr "يىل %1"

#: ../qml/OnlineAccountsHelper.qml:39
msgid "Pick an account to create."
msgstr ""

#: ../qml/CalendarChoicePopup.qml:44 ../qml/EventActions.qml:54
msgid "Calendars"
msgstr "يىلنامىلەر"

#: ../qml/CalendarChoicePopup.qml:46 ../qml/SettingsPage.qml:51
msgid "Back"
msgstr "كەينى"

#. TRANSLATORS: Please translate this string  to 15 characters only.
#. Currently ,there is no way we can increase width of action menu currently.
#: ../qml/CalendarChoicePopup.qml:58 ../qml/EventActions.qml:39
msgid "Sync"
msgstr "قەدەمداشلا"

#: ../qml/CalendarChoicePopup.qml:58 ../qml/EventActions.qml:39
msgid "Syncing"
msgstr "قەدەمداشلاۋاتىدۇ"

#: ../qml/CalendarChoicePopup.qml:83
msgid "Add online Calendar"
msgstr "تور يىلنامىسى قۇشۇش"

#: ../qml/CalendarChoicePopup.qml:184
msgid "Unable to deselect"
msgstr ""

#: ../qml/CalendarChoicePopup.qml:185
msgid ""
"In order to create new events you must have at least one writable calendar "
"selected"
msgstr ""

#: ../qml/CalendarChoicePopup.qml:187
msgid "Ok"
msgstr "جەزىملە"

#: ../qml/RemindersPage.qml:62
msgid "Custom reminder"
msgstr "ئەسكەرتمە"

#: ../qml/RemindersPage.qml:73
#, fuzzy
msgid "Set reminder"
msgstr "ئەسكەرتمە"

#: ../qml/NewEvent.qml:206
msgid "End time can't be before start time"
msgstr "ئاخىرلىشىش ۋاقتى باشلىنىش ۋاقتىدىن بۇرۇن بولسا بولمايدۇ"

#: ../qml/NewEvent.qml:389 ../qml/EditEventConfirmationDialog.qml:29
msgid "Edit Event"
msgstr "ھادىسە تەھرىرلە"

#: ../qml/NewEvent.qml:389 ../qml/NewEventBottomEdge.qml:54
msgid "New Event"
msgstr "يېڭى ھادىسە"

#: ../qml/NewEvent.qml:401 ../qml/DeleteConfirmationDialog.qml:51
msgid "Delete"
msgstr "ئۆچۈر"

#: ../qml/NewEvent.qml:419
msgid "Save"
msgstr "ساقلا"

#: ../qml/NewEvent.qml:430
msgid "Error"
msgstr "خاتالىق"

#: ../qml/NewEvent.qml:432
msgid "OK"
msgstr "تامام"

#: ../qml/NewEvent.qml:494
msgid "From"
msgstr "يوللىغۇچى"

#: ../qml/NewEvent.qml:519
msgid "To"
msgstr "تاپشۇرۇپ ئالغۇچى"

#: ../qml/NewEvent.qml:548
msgid "All day event"
msgstr "پۈتۈن كۈنلۈك ھادىسە"

#: ../qml/NewEvent.qml:585
msgid "Event Name"
msgstr "ھادىسە ئاتى"

#: ../qml/NewEvent.qml:596 ../qml/EventDetails.qml:37
msgid "Event Details"
msgstr "ھادىسە تەپسىلاتلىرى"

#: ../qml/NewEvent.qml:605
#, fuzzy
msgid "More details"
msgstr "ھادىسە تەپسىلاتلىرى"

#: ../qml/NewEvent.qml:632 ../qml/EventDetails.qml:437
msgid "Description"
msgstr "چۈشەندۈرۈلۈشى"

#: ../qml/NewEvent.qml:656
msgid "Location"
msgstr "ئورنى"

#: ../qml/NewEvent.qml:677 ../qml/EventDetails.qml:348
#: com.ubuntu.calendar_calendar.desktop.in.h:1
msgid "Calendar"
msgstr "يىلنامە"

#: ../qml/NewEvent.qml:744
msgid "Guests"
msgstr "مېھمانلار"

#: ../qml/NewEvent.qml:754
msgid "Add Guest"
msgstr "مېھمان قوش"

#: ../qml/NewEvent.qml:867 ../qml/NewEvent.qml:884 ../qml/EventDetails.qml:464
msgid "Reminder"
msgstr "ئەسكەرتكۈچ"

#: ../qml/DeleteConfirmationDialog.qml:31
msgid "Delete Recurring Event"
msgstr "قايتىلانغان ھادىسىنى ئۆچۈرۈش"

#: ../qml/DeleteConfirmationDialog.qml:32
msgid "Delete Event"
msgstr "%1 ھادىسىنى ئۆچۈرۈش"

#. TRANSLATORS: argument (%1) refers to an event name.
#: ../qml/DeleteConfirmationDialog.qml:36
msgid "Delete only this event \"%1\", or all events in the series?"
msgstr ""
"پەقەت مۇشۇ ھادىسە«%1» نى ئۆچۈرسۇنمۇ ياكى مەزكۇر توپتىكى ھەممە ھادىسىنى "
"ئۆچۈرسۇنمۇ؟"

#: ../qml/DeleteConfirmationDialog.qml:37
msgid "Are you sure you want to delete the event \"%1\"?"
msgstr "ھادىسە «%1» نى راستلا ئۆچۈرەمسىز؟"

#: ../qml/DeleteConfirmationDialog.qml:40
msgid "Delete series"
msgstr "توپنى ئۆچۈر"

#: ../qml/DeleteConfirmationDialog.qml:51
msgid "Delete this"
msgstr "بۇنى ئۆچۈر"

#. TRANSLATORS: the argument refers to multiple recurrence of event with count .
#. E.g. "Daily; 5 times."
#: ../qml/EventUtils.qml:75
msgid "%1; %2 time"
msgid_plural "%1; %2 times"
msgstr[0] "%1؛ %2 قېتىم"

#. TRANSLATORS: the argument refers to recurrence until user selected date.
#. E.g. "Daily; until 12/12/2014."
#: ../qml/EventUtils.qml:79
msgid "%1; until %2"
msgstr "%1؛ %2 غىچە"

#: ../qml/EventUtils.qml:93
msgid "; every %1 days"
msgstr ""

#: ../qml/EventUtils.qml:95
msgid "; every %1 weeks"
msgstr ""

#: ../qml/EventUtils.qml:97
msgid "; every %1 months"
msgstr ""

#: ../qml/EventUtils.qml:99
msgid "; every %1 years"
msgstr ""

#. TRANSLATORS: the argument refers to several different days of the week.
#. E.g. "Weekly on Mondays, Tuesdays"
#: ../qml/EventUtils.qml:125
msgid "Weekly on %1"
msgstr "ھەر ھەپتىنىڭ %1 كۈنلىرى"

#: ../qml/WeekView.qml:140 ../qml/MonthView.qml:79
msgid "%1 %2"
msgstr ""

#: ../qml/WeekView.qml:147 ../qml/WeekView.qml:148
msgid "MMM"
msgstr ""

#. TRANSLATORS: this is a time formatting string,
#. see http://qt-project.org/doc/qt-5/qml-qtqml-date.html#details for valid expressions.
#. It's used in the header of the month and week views
#: ../qml/WeekView.qml:159 ../qml/DayView.qml:129 ../qml/MonthView.qml:84
msgid "MMMM yyyy"
msgstr "MMMM yyyy"

#. TRANSLATORS: argument (%1) refers to an event name.
#: ../qml/EditEventConfirmationDialog.qml:32
msgid "Edit only this event \"%1\", or all events in the series?"
msgstr ""
"پەقەت مۇشۇ ھادىسە«%1» نى تەھرىرلىسۇنمۇ ياكى مەزكۇر توپتىكى ھەممە ھادىسىنى "
"تەھرىرلىسۇنمۇ؟"

#: ../qml/EditEventConfirmationDialog.qml:35
msgid "Edit series"
msgstr "توپنى تەھرىرلە"

#: ../qml/EditEventConfirmationDialog.qml:44
msgid "Edit this"
msgstr "بۇنى تەھرىرلە"

#: ../qml/EventActions.qml:66 ../qml/SettingsPage.qml:49
msgid "Settings"
msgstr ""

#. TRANSLATORS: This is shown in the month view as "Wk" as a title
#. to indicate the week numbers. It should be a max of up to 3 characters.
#: ../qml/MonthComponent.qml:294
msgid "Wk"
msgstr ""

#: ../qml/ContactChoicePopup.qml:37
msgid "No contact"
msgstr "ئالاقەداش يوق"

#: ../qml/ContactChoicePopup.qml:96
msgid "Search contact"
msgstr "ئالاقەداش ئىزدە"

#: ../qml/EventDetails.qml:40
msgid "Edit"
msgstr "تەھرىرلە"

#: ../qml/EventDetails.qml:392
msgid "Attending"
msgstr ""

#: ../qml/EventDetails.qml:394
msgid "Not Attending"
msgstr ""

#: ../qml/EventDetails.qml:396
msgid "Maybe"
msgstr ""

#: ../qml/EventDetails.qml:398
msgid "No Reply"
msgstr ""

#. TRANSLATORS: W refers to Week, followed by the actual week number (%1)
#: ../qml/TimeLineHeader.qml:54
msgid "W%1"
msgstr "W%1"

#: ../qml/SettingsPage.qml:85
msgid "Show week numbers"
msgstr ""

#: ../qml/SettingsPage.qml:104
msgid "Display Chinese calendar"
msgstr ""

#: ../qml/SettingsPage.qml:125
msgid "Business hours"
msgstr ""

#: ../qml/SettingsPage.qml:235
msgid "Default reminder"
msgstr ""

#: ../qml/SettingsPage.qml:282
msgid "Default length of new event"
msgstr ""

#: ../qml/SettingsPage.qml:345
msgid "Default calendar"
msgstr ""

#: ../qml/SettingsPage.qml:414
msgid "Theme"
msgstr ""

#: ../qml/SettingsPage.qml:437
msgid "System theme"
msgstr ""

#: ../qml/SettingsPage.qml:438
msgid "SuruDark theme"
msgstr ""

#: ../qml/SettingsPage.qml:439
msgid "Ambiance theme"
msgstr ""

#: ../qml/LimitLabelModel.qml:25
msgid "Never"
msgstr "ھەرگىز"

#: ../qml/LimitLabelModel.qml:26
msgid "After X Occurrence"
msgstr "X قېتىم تەكرارلانغاندىن كېيىن"

#: ../qml/LimitLabelModel.qml:27
msgid "After Date"
msgstr "چېسلا كېيىن"

#. TRANSLATORS: the first argument (%1) refers to a start time for an event,
#. while the second one (%2) refers to the end time
#: ../qml/EventBubble.qml:139
msgid "%1 - %2"
msgstr "%1 - %2"

#: ../qml/RecurrenceLabelDefines.qml:23
msgid "Once"
msgstr "بىر قېتىملىق"

#: ../qml/RecurrenceLabelDefines.qml:24
msgid "Daily"
msgstr "كۈندە"

#: ../qml/RecurrenceLabelDefines.qml:25
msgid "On Weekdays"
msgstr "بىنەپتە كۈنلىرى"

#. TRANSLATORS: The arguments refer to days of the week. E.g. "On Monday, Tuesday, Thursday"
#: ../qml/RecurrenceLabelDefines.qml:27
msgid "On %1, %2 ,%3"
msgstr "%1،%2،%3 كۈنلىرى"

#. TRANSLATORS: The arguments refer to days of the week. E.g. "On Monday and Thursday"
#: ../qml/RecurrenceLabelDefines.qml:29
msgid "On %1 and %2"
msgstr "%1 ۋە %2 كۈنلىرى"

#: ../qml/RecurrenceLabelDefines.qml:30
msgid "Weekly"
msgstr "ھەر ھەپتىدە"

#: ../qml/RecurrenceLabelDefines.qml:31
msgid "Monthly"
msgstr "ھەر ئايدا"

#: ../qml/RecurrenceLabelDefines.qml:32
msgid "Yearly"
msgstr "ھەر يىلى"

#: com.ubuntu.calendar_calendar.desktop.in.h:2
msgid "A calendar for Ubuntu which syncs with online accounts."
msgstr "توردىكى ھېساباتلار بىلەن قەدەمداشلىغىلى بولىدىغان ئۇبۇنتۇ يىلنامىسى."

#: com.ubuntu.calendar_calendar.desktop.in.h:3
msgid "calendar;event;day;week;year;appointment;meeting;"
msgstr "يىلنامە;ھادىسە;كۈن;ھەپتە;يىل;دېيىشمە;يىغىن;"

#~ msgid "5 minutes"
#~ msgstr "5 مىنۇت"

#~ msgid "30 minutes"
#~ msgstr "30 مىنۇت"

#~ msgid "2 hours"
#~ msgstr "2 سائەت"

#~ msgid "2 days"
#~ msgstr "2 كۈن"

#~ msgid "2 weeks"
#~ msgstr "2 ھەپتە"

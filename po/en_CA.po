# English (Canada) translation for ubuntu-calendar-app
# Copyright (c) 2013 Rosetta Contributors and Canonical Ltd 2013
# This file is distributed under the same license as the ubuntu-calendar-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: ubuntu-calendar-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-04-20 04:27+0000\n"
"PO-Revision-Date: 2020-02-28 05:21+0000\n"
"Last-Translator: Ryan Argente <rargente@lonerider.one>\n"
"Language-Team: English (Canada) <https://translate.ubports.com/projects/"
"ubports/calendar-app/en_CA/>\n"
"Language: en_CA\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 3.8\n"
"X-Launchpad-Export-Date: 2017-04-05 07:14+0000\n"

#: ../qml/AllDayEventComponent.qml:89 ../qml/TimeLineBase.qml:50
msgid "New event"
msgstr "New event"

#. TRANSLATORS: Please keep the translation of this string to a max of
#. 5 characters as the week view where it is shown has limited space.
#: ../qml/AllDayEventComponent.qml:148
msgid "%1 event"
msgid_plural "%1 events"
msgstr[0] "%1 event"
msgstr[1] "%1 events"

#. TRANSLATORS: the argument refers to the number of all day events
#: ../qml/AllDayEventComponent.qml:152
msgid "%1 all day event"
msgid_plural "%1 all day events"
msgstr[0] "%1 all day event"
msgstr[1] "%1 all day events"

#: ../qml/RemindersModel.qml:31 ../qml/RemindersModel.qml:99
msgid "No Reminder"
msgstr "No Reminder"

#. TRANSLATORS: this refers to when a reminder should be shown as a notification
#. in the indicators. "On Event" means that it will be shown right at the time
#. the event starts, not any time before
#: ../qml/RemindersModel.qml:34 ../qml/RemindersModel.qml:103
msgid "On Event"
msgstr "On Event"

#: ../qml/RemindersModel.qml:43 ../qml/RemindersModel.qml:112
msgid "%1 week"
msgid_plural "%1 weeks"
msgstr[0] "%1 week"
msgstr[1] "%1 weeks"

#: ../qml/RemindersModel.qml:54 ../qml/RemindersModel.qml:110
msgid "%1 day"
msgid_plural "%1 days"
msgstr[0] "%1 day"
msgstr[1] "%1 days"

#: ../qml/RemindersModel.qml:65 ../qml/RemindersModel.qml:108
msgid "%1 hour"
msgid_plural "%1 hours"
msgstr[0] "%1 hour"
msgstr[1] "%1 hours"

#: ../qml/RemindersModel.qml:74
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] "%1 minute"
msgstr[1] "%1 minutes"

#: ../qml/RemindersModel.qml:104 ../qml/RemindersModel.qml:105
#: ../qml/RemindersModel.qml:106 ../qml/RemindersModel.qml:107
#: ../qml/SettingsPage.qml:304 ../qml/SettingsPage.qml:305
#: ../qml/SettingsPage.qml:306 ../qml/SettingsPage.qml:307
#: ../qml/SettingsPage.qml:308 ../qml/SettingsPage.qml:309
#: ../qml/SettingsPage.qml:310 ../qml/SettingsPage.qml:311
#, fuzzy
msgid "%1 minutes"
msgstr "%1 minute"

#: ../qml/RemindersModel.qml:109
#, fuzzy
msgid "%1 hours"
msgstr "%1 hour"

#: ../qml/RemindersModel.qml:111
#, fuzzy
msgid "%1 days"
msgstr "%1 day"

#: ../qml/RemindersModel.qml:113
#, fuzzy
msgid "%1 weeks"
msgstr "%1 week"

#: ../qml/RemindersModel.qml:114
msgid "Custom"
msgstr "Custom"

#: ../qml/calendar.qml:80
msgid ""
"Calendar app accept four arguments: --starttime, --endtime, --newevent and --"
"eventid. They will be managed by system. See the source for a full comment "
"about them"
msgstr ""
"Calendar app accepts four arguments: --starttime, --endtime, --newevent and "
"--eventid. They will be managed by system. See the source for a full comment "
"about them"

#: ../qml/calendar.qml:341 ../qml/calendar.qml:522 ../qml/AgendaView.qml:51
msgid "Agenda"
msgstr "Agenda"

#: ../qml/calendar.qml:349 ../qml/calendar.qml:543
msgid "Day"
msgstr "Day"

#: ../qml/calendar.qml:357 ../qml/calendar.qml:564
msgid "Week"
msgstr "Week"

#: ../qml/calendar.qml:365 ../qml/calendar.qml:585
msgid "Month"
msgstr "Month"

#: ../qml/calendar.qml:373 ../qml/calendar.qml:606
msgid "Year"
msgstr "Year"

#: ../qml/calendar.qml:718 ../qml/EventDetails.qml:173
#: ../qml/TimeLineHeader.qml:66
msgid "All Day"
msgstr "All Day"

#: ../qml/ColorPickerDialog.qml:25
msgid "Select Color"
msgstr "Select Colour"

#: ../qml/ColorPickerDialog.qml:55 ../qml/OnlineAccountsHelper.qml:73
#: ../qml/RemindersPage.qml:88 ../qml/NewEvent.qml:394
#: ../qml/DeleteConfirmationDialog.qml:63
#: ../qml/EditEventConfirmationDialog.qml:52
msgid "Cancel"
msgstr "Cancel"

#: ../qml/AgendaView.qml:101
msgid "You have no calendars enabled"
msgstr "You have no calendars enabled"

#: ../qml/AgendaView.qml:101
msgid "No upcoming events"
msgstr "No upcoming events"

#: ../qml/AgendaView.qml:113
msgid "Enable calendars"
msgstr "Enable calendars"

#: ../qml/AgendaView.qml:206
msgid "no event name set"
msgstr "no event name set"

#: ../qml/AgendaView.qml:208
msgid "no location"
msgstr "no location"

#. TRANSLATORS: this refers to how often a recurrent event repeats
#. and it is shown as the header of the page to choose repetition
#. and as the header of the list item that shows the repetition
#. summary in the page that displays the event details
#: ../qml/EventRepetition.qml:40 ../qml/EventRepetition.qml:179
msgid "Repeat"
msgstr "Repeat"

#: ../qml/EventRepetition.qml:199
msgid "Repeats On:"
msgstr "Repeats On:"

#: ../qml/EventRepetition.qml:245
msgid "Interval of recurrence"
msgstr "Interval of recurrence"

#: ../qml/EventRepetition.qml:270
msgid "Recurring event ends"
msgstr "Recurring event ends"

#. TRANSLATORS: this refers to how often a recurrent event repeats
#. and it is shown as the header of the option selector to choose
#. its repetition
#: ../qml/EventRepetition.qml:294 ../qml/NewEvent.qml:840
msgid "Repeats"
msgstr "Repeats"

#: ../qml/EventRepetition.qml:320 ../qml/NewEvent.qml:494
msgid "Date"
msgstr "Date"

#: ../qml/YearView.qml:57 ../qml/WeekView.qml:60 ../qml/DayView.qml:76
#: ../qml/MonthView.qml:51
msgid "Today"
msgstr "Today"

#: ../qml/YearView.qml:83
msgid "Year %1"
msgstr "Year %1"

#: ../qml/OnlineAccountsHelper.qml:39
msgid "Pick an account to create."
msgstr "Pick an account to create."

#: ../qml/CalendarChoicePopup.qml:44 ../qml/EventActions.qml:54
msgid "Calendars"
msgstr "Calendars"

#: ../qml/CalendarChoicePopup.qml:46 ../qml/SettingsPage.qml:51
msgid "Back"
msgstr "Back"

#. TRANSLATORS: Please translate this string  to 15 characters only.
#. Currently ,there is no way we can increase width of action menu currently.
#: ../qml/CalendarChoicePopup.qml:58 ../qml/EventActions.qml:39
msgid "Sync"
msgstr "Sync"

#: ../qml/CalendarChoicePopup.qml:58 ../qml/EventActions.qml:39
msgid "Syncing"
msgstr "Syncing"

#: ../qml/CalendarChoicePopup.qml:83
msgid "Add online Calendar"
msgstr "Add online Calendar"

#: ../qml/CalendarChoicePopup.qml:184
msgid "Unable to deselect"
msgstr "Unable to deselect"

#: ../qml/CalendarChoicePopup.qml:185
msgid ""
"In order to create new events you must have at least one writable calendar "
"selected"
msgstr ""
"In order to create new events you must have at least one writable calendar "
"selected"

#: ../qml/CalendarChoicePopup.qml:187
msgid "Ok"
msgstr "Ok"

#: ../qml/RemindersPage.qml:62
msgid "Custom reminder"
msgstr "Custom reminder"

#: ../qml/RemindersPage.qml:73
#, fuzzy
msgid "Set reminder"
msgstr "Default reminder"

#: ../qml/NewEvent.qml:206
msgid "End time can't be before start time"
msgstr "End time can't be before start time"

#: ../qml/NewEvent.qml:389 ../qml/EditEventConfirmationDialog.qml:29
msgid "Edit Event"
msgstr "Edit Event"

#: ../qml/NewEvent.qml:389 ../qml/NewEventBottomEdge.qml:54
msgid "New Event"
msgstr "New Event"

#: ../qml/NewEvent.qml:401 ../qml/DeleteConfirmationDialog.qml:51
msgid "Delete"
msgstr "Delete"

#: ../qml/NewEvent.qml:419
msgid "Save"
msgstr "Save"

#: ../qml/NewEvent.qml:430
msgid "Error"
msgstr "Error"

#: ../qml/NewEvent.qml:432
msgid "OK"
msgstr "OK"

#: ../qml/NewEvent.qml:494
msgid "From"
msgstr "From"

#: ../qml/NewEvent.qml:519
msgid "To"
msgstr "To"

#: ../qml/NewEvent.qml:548
msgid "All day event"
msgstr "All day event"

#: ../qml/NewEvent.qml:585
msgid "Event Name"
msgstr "Event Name"

#: ../qml/NewEvent.qml:596 ../qml/EventDetails.qml:37
msgid "Event Details"
msgstr "Event Details"

#: ../qml/NewEvent.qml:605
#, fuzzy
msgid "More details"
msgstr "Event Details"

#: ../qml/NewEvent.qml:632 ../qml/EventDetails.qml:437
msgid "Description"
msgstr "Description"

#: ../qml/NewEvent.qml:656
msgid "Location"
msgstr "Location"

#: ../qml/NewEvent.qml:677 ../qml/EventDetails.qml:348
#: com.ubuntu.calendar_calendar.desktop.in.h:1
msgid "Calendar"
msgstr "Calendar"

#: ../qml/NewEvent.qml:744
msgid "Guests"
msgstr "Guests"

#: ../qml/NewEvent.qml:754
msgid "Add Guest"
msgstr "Add Guest"

#: ../qml/NewEvent.qml:867 ../qml/NewEvent.qml:884 ../qml/EventDetails.qml:464
msgid "Reminder"
msgstr "Reminder"

#: ../qml/DeleteConfirmationDialog.qml:31
msgid "Delete Recurring Event"
msgstr "Delete Recurring Event"

#: ../qml/DeleteConfirmationDialog.qml:32
msgid "Delete Event"
msgstr "Delete Event"

#. TRANSLATORS: argument (%1) refers to an event name.
#: ../qml/DeleteConfirmationDialog.qml:36
msgid "Delete only this event \"%1\", or all events in the series?"
msgstr "Delete only this event \"%1\", or all events in the series?"

#: ../qml/DeleteConfirmationDialog.qml:37
msgid "Are you sure you want to delete the event \"%1\"?"
msgstr "Are you sure you want to delete the event \"%1\"?"

#: ../qml/DeleteConfirmationDialog.qml:40
msgid "Delete series"
msgstr "Delete series"

#: ../qml/DeleteConfirmationDialog.qml:51
msgid "Delete this"
msgstr "Delete this"

#. TRANSLATORS: the argument refers to multiple recurrence of event with count .
#. E.g. "Daily; 5 times."
#: ../qml/EventUtils.qml:75
msgid "%1; %2 time"
msgid_plural "%1; %2 times"
msgstr[0] "%1; %2 time"
msgstr[1] "%1; %2 times"

#. TRANSLATORS: the argument refers to recurrence until user selected date.
#. E.g. "Daily; until 12/12/2014."
#: ../qml/EventUtils.qml:79
msgid "%1; until %2"
msgstr "%1; until %2"

#: ../qml/EventUtils.qml:93
msgid "; every %1 days"
msgstr "; every %1 days"

#: ../qml/EventUtils.qml:95
msgid "; every %1 weeks"
msgstr "; every %1 weeks"

#: ../qml/EventUtils.qml:97
msgid "; every %1 months"
msgstr "; every %1 months"

#: ../qml/EventUtils.qml:99
msgid "; every %1 years"
msgstr "; every %1 years"

#. TRANSLATORS: the argument refers to several different days of the week.
#. E.g. "Weekly on Mondays, Tuesdays"
#: ../qml/EventUtils.qml:125
msgid "Weekly on %1"
msgstr "Weekly on %1"

#: ../qml/WeekView.qml:140 ../qml/MonthView.qml:79
msgid "%1 %2"
msgstr "%1 %2"

#: ../qml/WeekView.qml:147 ../qml/WeekView.qml:148
msgid "MMM"
msgstr "MMM"

#. TRANSLATORS: this is a time formatting string,
#. see http://qt-project.org/doc/qt-5/qml-qtqml-date.html#details for valid expressions.
#. It's used in the header of the month and week views
#: ../qml/WeekView.qml:159 ../qml/DayView.qml:129 ../qml/MonthView.qml:84
msgid "MMMM yyyy"
msgstr "MMMM yyyy"

#. TRANSLATORS: argument (%1) refers to an event name.
#: ../qml/EditEventConfirmationDialog.qml:32
msgid "Edit only this event \"%1\", or all events in the series?"
msgstr "Edit only this event \"%1\", or all events in the series?"

#: ../qml/EditEventConfirmationDialog.qml:35
msgid "Edit series"
msgstr "Edit series"

#: ../qml/EditEventConfirmationDialog.qml:44
msgid "Edit this"
msgstr "Edit this"

#: ../qml/EventActions.qml:66 ../qml/SettingsPage.qml:49
msgid "Settings"
msgstr "Settings"

#. TRANSLATORS: This is shown in the month view as "Wk" as a title
#. to indicate the week numbers. It should be a max of up to 3 characters.
#: ../qml/MonthComponent.qml:294
msgid "Wk"
msgstr "Wk"

#: ../qml/ContactChoicePopup.qml:37
msgid "No contact"
msgstr "No contact"

#: ../qml/ContactChoicePopup.qml:96
msgid "Search contact"
msgstr "Search contact"

#: ../qml/EventDetails.qml:40
msgid "Edit"
msgstr "Edit"

#: ../qml/EventDetails.qml:392
msgid "Attending"
msgstr "Attending"

#: ../qml/EventDetails.qml:394
msgid "Not Attending"
msgstr "Not Attending"

#: ../qml/EventDetails.qml:396
msgid "Maybe"
msgstr "Maybe"

#: ../qml/EventDetails.qml:398
msgid "No Reply"
msgstr "No Reply"

#. TRANSLATORS: W refers to Week, followed by the actual week number (%1)
#: ../qml/TimeLineHeader.qml:54
msgid "W%1"
msgstr "W%1"

#: ../qml/SettingsPage.qml:85
msgid "Show week numbers"
msgstr "Show week numbers"

#: ../qml/SettingsPage.qml:104
msgid "Display Chinese calendar"
msgstr "Display Chinese calendar"

#: ../qml/SettingsPage.qml:125
msgid "Business hours"
msgstr "Business hours"

#: ../qml/SettingsPage.qml:235
msgid "Default reminder"
msgstr "Default reminder"

#: ../qml/SettingsPage.qml:282
msgid "Default length of new event"
msgstr ""

#: ../qml/SettingsPage.qml:345
msgid "Default calendar"
msgstr "Default calendar"

#: ../qml/SettingsPage.qml:414
msgid "Theme"
msgstr ""

#: ../qml/SettingsPage.qml:437
msgid "System theme"
msgstr ""

#: ../qml/SettingsPage.qml:438
msgid "SuruDark theme"
msgstr ""

#: ../qml/SettingsPage.qml:439
msgid "Ambiance theme"
msgstr ""

#: ../qml/LimitLabelModel.qml:25
msgid "Never"
msgstr "Never"

#: ../qml/LimitLabelModel.qml:26
msgid "After X Occurrence"
msgstr "After X Occurrence"

#: ../qml/LimitLabelModel.qml:27
msgid "After Date"
msgstr "After Date"

#. TRANSLATORS: the first argument (%1) refers to a start time for an event,
#. while the second one (%2) refers to the end time
#: ../qml/EventBubble.qml:139
msgid "%1 - %2"
msgstr "%1 - %2"

#: ../qml/RecurrenceLabelDefines.qml:23
msgid "Once"
msgstr "Once"

#: ../qml/RecurrenceLabelDefines.qml:24
msgid "Daily"
msgstr "Daily"

#: ../qml/RecurrenceLabelDefines.qml:25
msgid "On Weekdays"
msgstr "On Weekdays"

#. TRANSLATORS: The arguments refer to days of the week. E.g. "On Monday, Tuesday, Thursday"
#: ../qml/RecurrenceLabelDefines.qml:27
msgid "On %1, %2 ,%3"
msgstr "On %1, %2 ,%3"

#. TRANSLATORS: The arguments refer to days of the week. E.g. "On Monday and Thursday"
#: ../qml/RecurrenceLabelDefines.qml:29
msgid "On %1 and %2"
msgstr "On %1 and %2"

#: ../qml/RecurrenceLabelDefines.qml:30
msgid "Weekly"
msgstr "Weekly"

#: ../qml/RecurrenceLabelDefines.qml:31
msgid "Monthly"
msgstr "Monthly"

#: ../qml/RecurrenceLabelDefines.qml:32
msgid "Yearly"
msgstr "Yearly"

#: com.ubuntu.calendar_calendar.desktop.in.h:2
msgid "A calendar for Ubuntu which syncs with online accounts."
msgstr "A calendar for Ubuntu which syncs with online accounts."

#: com.ubuntu.calendar_calendar.desktop.in.h:3
msgid "calendar;event;day;week;year;appointment;meeting;"
msgstr "calendar;event;day;week;year;appointment;meeting;"

#~ msgid "5 minutes"
#~ msgstr "5 minutes"

#~ msgid "10 minutes"
#~ msgstr "10 minutes"

#~ msgid "15 minutes"
#~ msgstr "15 minutes"

#~ msgid "30 minutes"
#~ msgstr "30 minutes"

#~ msgid "1 hour"
#~ msgstr "1 hour"

#~ msgid "2 hours"
#~ msgstr "2 hours"

#~ msgid "1 day"
#~ msgstr "1 day"

#~ msgid "2 days"
#~ msgstr "2 days"

#~ msgid "1 week"
#~ msgstr "1 week"

#~ msgid "2 weeks"
#~ msgstr "2 weeks"

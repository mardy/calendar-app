# Sardinian translation for ubuntu-calendar-app
# Copyright (c) 2015 Rosetta Contributors and Canonical Ltd 2015
# This file is distributed under the same license as the ubuntu-calendar-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: ubuntu-calendar-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-04-20 04:27+0000\n"
"PO-Revision-Date: 2021-11-21 15:09+0000\n"
"Last-Translator: Adria <adriamartinmor@gmail.com>\n"
"Language-Team: Sardinian <https://translate.ubports.com/projects/ubports/"
"calendar-app/sc/>\n"
"Language: sc\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 3.11.3\n"
"X-Launchpad-Export-Date: 2017-04-05 07:14+0000\n"

#: ../qml/AllDayEventComponent.qml:89 ../qml/TimeLineBase.qml:50
msgid "New event"
msgstr "Eventu nou"

#. TRANSLATORS: Please keep the translation of this string to a max of
#. 5 characters as the week view where it is shown has limited space.
#: ../qml/AllDayEventComponent.qml:148
msgid "%1 event"
msgid_plural "%1 events"
msgstr[0] "%1 eventu"
msgstr[1] "%1 eventos"

#. TRANSLATORS: the argument refers to the number of all day events
#: ../qml/AllDayEventComponent.qml:152
msgid "%1 all day event"
msgid_plural "%1 all day events"
msgstr[0] "%1 eventu de totu sa die"
msgstr[1] "%1 eventos de totu sa die"

#: ../qml/RemindersModel.qml:31 ../qml/RemindersModel.qml:99
msgid "No Reminder"
msgstr "Sena apuntu"

#. TRANSLATORS: this refers to when a reminder should be shown as a notification
#. in the indicators. "On Event" means that it will be shown right at the time
#. the event starts, not any time before
#: ../qml/RemindersModel.qml:34 ../qml/RemindersModel.qml:103
msgid "On Event"
msgstr "In s'ora de s'eventu"

#: ../qml/RemindersModel.qml:43 ../qml/RemindersModel.qml:112
msgid "%1 week"
msgid_plural "%1 weeks"
msgstr[0] "%1 chida"
msgstr[1] "%1 chidas"

#: ../qml/RemindersModel.qml:54 ../qml/RemindersModel.qml:110
msgid "%1 day"
msgid_plural "%1 days"
msgstr[0] "%1 die"
msgstr[1] "%1 dies"

#: ../qml/RemindersModel.qml:65 ../qml/RemindersModel.qml:108
msgid "%1 hour"
msgid_plural "%1 hours"
msgstr[0] "%1 ora"
msgstr[1] "%1 oras"

#: ../qml/RemindersModel.qml:74
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] "%1 minutu"
msgstr[1] "%1 minutos"

#: ../qml/RemindersModel.qml:104 ../qml/RemindersModel.qml:105
#: ../qml/RemindersModel.qml:106 ../qml/RemindersModel.qml:107
#: ../qml/SettingsPage.qml:304 ../qml/SettingsPage.qml:305
#: ../qml/SettingsPage.qml:306 ../qml/SettingsPage.qml:307
#: ../qml/SettingsPage.qml:308 ../qml/SettingsPage.qml:309
#: ../qml/SettingsPage.qml:310 ../qml/SettingsPage.qml:311
msgid "%1 minutes"
msgstr "%1 minutos"

#: ../qml/RemindersModel.qml:109
msgid "%1 hours"
msgstr "%1 oras"

#: ../qml/RemindersModel.qml:111
msgid "%1 days"
msgstr "%1 dies"

#: ../qml/RemindersModel.qml:113
msgid "%1 weeks"
msgstr "%1 chidas"

#: ../qml/RemindersModel.qml:114
msgid "Custom"
msgstr "Personalizadu"

#: ../qml/calendar.qml:80
msgid ""
"Calendar app accept four arguments: --starttime, --endtime, --newevent and --"
"eventid. They will be managed by system. See the source for a full comment "
"about them"
msgstr ""
"S'aplicatzione Calendàriu ammitet bator argumentos: --starttime, --endtime, "
"--newevent and --eventid. Ant a èssere gestidos dae su sistema. Càstia su "
"còdighe de orìgine pro àteras informatziones"

#: ../qml/calendar.qml:341 ../qml/calendar.qml:522 ../qml/AgendaView.qml:51
msgid "Agenda"
msgstr "Agenda"

#: ../qml/calendar.qml:349 ../qml/calendar.qml:543
msgid "Day"
msgstr "Die"

#: ../qml/calendar.qml:357 ../qml/calendar.qml:564
msgid "Week"
msgstr "Chida"

#: ../qml/calendar.qml:365 ../qml/calendar.qml:585
msgid "Month"
msgstr "Mese"

#: ../qml/calendar.qml:373 ../qml/calendar.qml:606
msgid "Year"
msgstr "Annu"

#: ../qml/calendar.qml:718 ../qml/EventDetails.qml:173
#: ../qml/TimeLineHeader.qml:66
msgid "All Day"
msgstr "Totu sa die"

#: ../qml/ColorPickerDialog.qml:25
msgid "Select Color"
msgstr "Seletziona colore"

#: ../qml/ColorPickerDialog.qml:55 ../qml/OnlineAccountsHelper.qml:73
#: ../qml/RemindersPage.qml:88 ../qml/NewEvent.qml:394
#: ../qml/DeleteConfirmationDialog.qml:63
#: ../qml/EditEventConfirmationDialog.qml:52
msgid "Cancel"
msgstr "Annulla"

#: ../qml/AgendaView.qml:101
msgid "You have no calendars enabled"
msgstr "Nissunu calendàriu ativadu"

#: ../qml/AgendaView.qml:101
msgid "No upcoming events"
msgstr "Nissunu eventu imbeniente"

#: ../qml/AgendaView.qml:113
msgid "Enable calendars"
msgstr "Ativa calendàrios"

#: ../qml/AgendaView.qml:206
msgid "no event name set"
msgstr "eventu sena nòmine"

#: ../qml/AgendaView.qml:208
msgid "no location"
msgstr "sena positzione"

#. TRANSLATORS: this refers to how often a recurrent event repeats
#. and it is shown as the header of the page to choose repetition
#. and as the header of the list item that shows the repetition
#. summary in the page that displays the event details
#: ../qml/EventRepetition.qml:40 ../qml/EventRepetition.qml:179
msgid "Repeat"
msgstr "Ripetitzione"

#: ../qml/EventRepetition.qml:199
msgid "Repeats On:"
msgstr "Si repìtet:"

#: ../qml/EventRepetition.qml:245
msgid "Interval of recurrence"
msgstr "Intervallu de ripetitzione"

#: ../qml/EventRepetition.qml:270
msgid "Recurring event ends"
msgstr "S'eventu ricurrente acabbat"

#. TRANSLATORS: this refers to how often a recurrent event repeats
#. and it is shown as the header of the option selector to choose
#. its repetition
#: ../qml/EventRepetition.qml:294 ../qml/NewEvent.qml:840
msgid "Repeats"
msgstr "Si repitet"

#: ../qml/EventRepetition.qml:320 ../qml/NewEvent.qml:494
msgid "Date"
msgstr "Data"

#: ../qml/YearView.qml:57 ../qml/WeekView.qml:60 ../qml/DayView.qml:76
#: ../qml/MonthView.qml:51
msgid "Today"
msgstr "Oe"

#: ../qml/YearView.qml:83
msgid "Year %1"
msgstr "Annu %1"

#: ../qml/OnlineAccountsHelper.qml:39
msgid "Pick an account to create."
msgstr "Sèbera unu contu de creare."

#: ../qml/CalendarChoicePopup.qml:44 ../qml/EventActions.qml:54
msgid "Calendars"
msgstr "Calendàrios"

#: ../qml/CalendarChoicePopup.qml:46 ../qml/SettingsPage.qml:51
msgid "Back"
msgstr "In segus"

#. TRANSLATORS: Please translate this string  to 15 characters only.
#. Currently ,there is no way we can increase width of action menu currently.
#: ../qml/CalendarChoicePopup.qml:58 ../qml/EventActions.qml:39
msgid "Sync"
msgstr "Sincroniza"

#: ../qml/CalendarChoicePopup.qml:58 ../qml/EventActions.qml:39
msgid "Syncing"
msgstr "Sincronizatzione"

#: ../qml/CalendarChoicePopup.qml:83
msgid "Add online Calendar"
msgstr "Agiunghe calendàriu in lìnia"

#: ../qml/CalendarChoicePopup.qml:184
msgid "Unable to deselect"
msgstr "Impossìbile deseletzionare"

#: ../qml/CalendarChoicePopup.qml:185
msgid ""
"In order to create new events you must have at least one writable calendar "
"selected"
msgstr ""
"Deves tènnere seletzionadu a su nessi unu calendàriu de iscritura pro pòdere "
"creare eventos noos"

#: ../qml/CalendarChoicePopup.qml:187
msgid "Ok"
msgstr "AB"

#: ../qml/RemindersPage.qml:62
msgid "Custom reminder"
msgstr "Apuntu personalizadu"

#: ../qml/RemindersPage.qml:73
msgid "Set reminder"
msgstr "Crea apuntu"

#: ../qml/NewEvent.qml:206
msgid "End time can't be before start time"
msgstr "S'ora de fine non podet èssere in antis de s'ora de cumintzu"

#: ../qml/NewEvent.qml:389 ../qml/EditEventConfirmationDialog.qml:29
msgid "Edit Event"
msgstr "Modìfica eventu"

#: ../qml/NewEvent.qml:389 ../qml/NewEventBottomEdge.qml:54
msgid "New Event"
msgstr "Eventu nou"

#: ../qml/NewEvent.qml:401 ../qml/DeleteConfirmationDialog.qml:51
msgid "Delete"
msgstr "Cantzella"

#: ../qml/NewEvent.qml:419
msgid "Save"
msgstr "Sarva"

#: ../qml/NewEvent.qml:430
msgid "Error"
msgstr "Faddina"

#: ../qml/NewEvent.qml:432
msgid "OK"
msgstr "AB"

#: ../qml/NewEvent.qml:494
msgid "From"
msgstr "Dae"

#: ../qml/NewEvent.qml:519
msgid "To"
msgstr "A"

#: ../qml/NewEvent.qml:548
msgid "All day event"
msgstr "Eventu de totu sa die"

#: ../qml/NewEvent.qml:585
msgid "Event Name"
msgstr "Nòmine de s'eventu"

#: ../qml/NewEvent.qml:596 ../qml/EventDetails.qml:37
msgid "Event Details"
msgstr "Detàllios de s'eventu"

#: ../qml/NewEvent.qml:605
msgid "More details"
msgstr "Àteros detàllios"

#: ../qml/NewEvent.qml:632 ../qml/EventDetails.qml:437
msgid "Description"
msgstr "Descritzione"

#: ../qml/NewEvent.qml:656
msgid "Location"
msgstr "Positzione"

#: ../qml/NewEvent.qml:677 ../qml/EventDetails.qml:348
#: com.ubuntu.calendar_calendar.desktop.in.h:1
msgid "Calendar"
msgstr "Calendàriu"

#: ../qml/NewEvent.qml:744
msgid "Guests"
msgstr "Invitados"

#: ../qml/NewEvent.qml:754
msgid "Add Guest"
msgstr "Agiunghe invitadu"

#: ../qml/NewEvent.qml:867 ../qml/NewEvent.qml:884 ../qml/EventDetails.qml:464
msgid "Reminder"
msgstr "Apuntu"

#: ../qml/DeleteConfirmationDialog.qml:31
msgid "Delete Recurring Event"
msgstr "Cantzella s'eventu ricurrente"

#: ../qml/DeleteConfirmationDialog.qml:32
msgid "Delete Event"
msgstr "Cantzella eventu"

#. TRANSLATORS: argument (%1) refers to an event name.
#: ../qml/DeleteConfirmationDialog.qml:36
msgid "Delete only this event \"%1\", or all events in the series?"
msgstr ""
"Boles cantzellare petzi custu eventu \"%1\", o totu is eventos in sa sèrie?"

#: ../qml/DeleteConfirmationDialog.qml:37
msgid "Are you sure you want to delete the event \"%1\"?"
msgstr "Ses seguru chi boles cantzellare s'eventu \"%1\"?"

#: ../qml/DeleteConfirmationDialog.qml:40
msgid "Delete series"
msgstr "Cantzella sa sèrie"

#: ../qml/DeleteConfirmationDialog.qml:51
msgid "Delete this"
msgstr "Cantzella custu"

#. TRANSLATORS: the argument refers to multiple recurrence of event with count .
#. E.g. "Daily; 5 times."
#: ../qml/EventUtils.qml:75
msgid "%1; %2 time"
msgid_plural "%1; %2 times"
msgstr[0] "%1; %2 borta"
msgstr[1] "%1; %2 bortas"

#. TRANSLATORS: the argument refers to recurrence until user selected date.
#. E.g. "Daily; until 12/12/2014."
#: ../qml/EventUtils.qml:79
msgid "%1; until %2"
msgstr "%1; finas a su %2"

#: ../qml/EventUtils.qml:93
msgid "; every %1 days"
msgstr "; cada %1 dies"

#: ../qml/EventUtils.qml:95
msgid "; every %1 weeks"
msgstr "; cada %1 chidas"

#: ../qml/EventUtils.qml:97
msgid "; every %1 months"
msgstr "; cada %1 meses"

#: ../qml/EventUtils.qml:99
msgid "; every %1 years"
msgstr "; cada %1 annos"

#. TRANSLATORS: the argument refers to several different days of the week.
#. E.g. "Weekly on Mondays, Tuesdays"
#: ../qml/EventUtils.qml:125
msgid "Weekly on %1"
msgstr "Cada chida su %1"

#: ../qml/WeekView.qml:140 ../qml/MonthView.qml:79
msgid "%1 %2"
msgstr "%1 de su %2"

#: ../qml/WeekView.qml:147 ../qml/WeekView.qml:148
msgid "MMM"
msgstr "MMM"

#. TRANSLATORS: this is a time formatting string,
#. see http://qt-project.org/doc/qt-5/qml-qtqml-date.html#details for valid expressions.
#. It's used in the header of the month and week views
#: ../qml/WeekView.qml:159 ../qml/DayView.qml:129 ../qml/MonthView.qml:84
msgid "MMMM yyyy"
msgstr "MMMM yyyy"

#. TRANSLATORS: argument (%1) refers to an event name.
#: ../qml/EditEventConfirmationDialog.qml:32
msgid "Edit only this event \"%1\", or all events in the series?"
msgstr ""
"Boles modificare isceti custu eventu \"%1\", o totus is eventos de sa sèrie?"

#: ../qml/EditEventConfirmationDialog.qml:35
msgid "Edit series"
msgstr "Modìfica sa sèrie"

#: ../qml/EditEventConfirmationDialog.qml:44
msgid "Edit this"
msgstr "Modìfica custu"

#: ../qml/EventActions.qml:66 ../qml/SettingsPage.qml:49
msgid "Settings"
msgstr "Cunfiguratziones"

#. TRANSLATORS: This is shown in the month view as "Wk" as a title
#. to indicate the week numbers. It should be a max of up to 3 characters.
#: ../qml/MonthComponent.qml:294
msgid "Wk"
msgstr "ch"

#: ../qml/ContactChoicePopup.qml:37
msgid "No contact"
msgstr "Nissunu cuntatu"

#: ../qml/ContactChoicePopup.qml:96
msgid "Search contact"
msgstr "Chirca cuntatu"

#: ../qml/EventDetails.qml:40
msgid "Edit"
msgstr "Modìfica"

#: ../qml/EventDetails.qml:392
msgid "Attending"
msgstr "Partètzipant"

#: ../qml/EventDetails.qml:394
msgid "Not Attending"
msgstr "Non partètzipant"

#: ../qml/EventDetails.qml:396
msgid "Maybe"
msgstr "Fortzes"

#: ../qml/EventDetails.qml:398
msgid "No Reply"
msgstr "Sena risposta"

#. TRANSLATORS: W refers to Week, followed by the actual week number (%1)
#: ../qml/TimeLineHeader.qml:54
msgid "W%1"
msgstr "Ch%1"

#: ../qml/SettingsPage.qml:85
msgid "Show week numbers"
msgstr "Ammustra nùmeros de chida"

#: ../qml/SettingsPage.qml:104
msgid "Display Chinese calendar"
msgstr "Ammustra calendàriu tzinesu"

#: ../qml/SettingsPage.qml:125
msgid "Business hours"
msgstr "Oras de traballu"

#: ../qml/SettingsPage.qml:235
msgid "Default reminder"
msgstr "Apuntu predefinidu"

#: ../qml/SettingsPage.qml:282
msgid "Default length of new event"
msgstr "Longària predefinida de is eventos noos"

#: ../qml/SettingsPage.qml:345
msgid "Default calendar"
msgstr "Calendàriu predefinidu"

#: ../qml/SettingsPage.qml:414
msgid "Theme"
msgstr "Tema"

#: ../qml/SettingsPage.qml:437
msgid "System theme"
msgstr "Tema de su sistema"

#: ../qml/SettingsPage.qml:438
msgid "SuruDark theme"
msgstr "Tema SuruDark"

#: ../qml/SettingsPage.qml:439
msgid "Ambiance theme"
msgstr "Tema Ambiance"

#: ../qml/LimitLabelModel.qml:25
msgid "Never"
msgstr "Mai"

#: ../qml/LimitLabelModel.qml:26
msgid "After X Occurrence"
msgstr "A pustis de X eventos"

#: ../qml/LimitLabelModel.qml:27
msgid "After Date"
msgstr "A pustis de sa data"

#. TRANSLATORS: the first argument (%1) refers to a start time for an event,
#. while the second one (%2) refers to the end time
#: ../qml/EventBubble.qml:139
msgid "%1 - %2"
msgstr "%1 - %2"

#: ../qml/RecurrenceLabelDefines.qml:23
msgid "Once"
msgstr "Una borta"

#: ../qml/RecurrenceLabelDefines.qml:24
msgid "Daily"
msgstr "Cada die"

#: ../qml/RecurrenceLabelDefines.qml:25
msgid "On Weekdays"
msgstr "A intre chida"

#. TRANSLATORS: The arguments refer to days of the week. E.g. "On Monday, Tuesday, Thursday"
#: ../qml/RecurrenceLabelDefines.qml:27
msgid "On %1, %2 ,%3"
msgstr "Su %1, %2, %2"

#. TRANSLATORS: The arguments refer to days of the week. E.g. "On Monday and Thursday"
#: ../qml/RecurrenceLabelDefines.qml:29
msgid "On %1 and %2"
msgstr "In su %1 e su %2"

#: ../qml/RecurrenceLabelDefines.qml:30
msgid "Weekly"
msgstr "Cada chida"

#: ../qml/RecurrenceLabelDefines.qml:31
msgid "Monthly"
msgstr "Cada mese"

#: ../qml/RecurrenceLabelDefines.qml:32
msgid "Yearly"
msgstr "Cada annu"

#: com.ubuntu.calendar_calendar.desktop.in.h:2
msgid "A calendar for Ubuntu which syncs with online accounts."
msgstr "Calendàriu pro Ubuntu chi sincronizat cun contos in lìnia."

#: com.ubuntu.calendar_calendar.desktop.in.h:3
msgid "calendar;event;day;week;year;appointment;meeting;"
msgstr "calendàriu;eventu;die;chida;annu;apuntamentu;riunione;"

#~ msgid "5 minutes"
#~ msgstr "5 minutos"

#~ msgid "10 minutes"
#~ msgstr "10 minutos"

#~ msgid "15 minutes"
#~ msgstr "15 minutos"

#~ msgid "30 minutes"
#~ msgstr "30 minutos"

#~ msgid "1 hour"
#~ msgstr "1 ora"

#~ msgid "2 hours"
#~ msgstr "2 oras"

#~ msgid "1 day"
#~ msgstr "1 die"

#~ msgid "2 days"
#~ msgstr "2 dies"

#~ msgid "1 week"
#~ msgstr "1 chida"

#~ msgid "2 weeks"
#~ msgstr "2 chidas"

#~ msgid "Show lunar calendar"
#~ msgstr "Ammustra calendàriu lunare"
